/*
 * 3-1.2.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

#include<iostream>
using namespace std;

int main(){

	int arg1;
	arg1=-1;
	{
		char arg1='A';
		cout<<arg1<<"\n";
	}
	return 0;
}


/* Yes the program now compiles with no error(s). The output was A. Since we have used brackets properly in this case, cout identify only arg1='A' to print. */


