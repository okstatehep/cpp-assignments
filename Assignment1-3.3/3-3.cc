/*
 * 3-3.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

/* This is to find prime numbers requested by user defined value N */

#include<iostream>
using namespace std;

int main(){

	int N;

	cout<<"enter the number of primes you want: ";
	cin>>N; // this is taking how many primes the user want

	for(int i=1; i<=N; ++i){

		int a;
		cin>>a; // this is taking the value given by the user

		// this is how the program decide the given number is a prime or not

		if(a%2==1){

			cout<<N<<" is a prime number"<<endl;
		}
		else{
			cout<<N<<" is not a prime number"<<endl;
		}
	}



	return 0;
}
