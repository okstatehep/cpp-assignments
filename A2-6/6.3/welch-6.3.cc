/*
 * 6.3.cc
 *
 *  Created on: Sep 16, 2015
 *      Author: thilak
 */

/* this is to transpose a given matrix */


#include<iostream>
 using namespace std;


 void transpose(const int input[][4], int output[][4])
 {
	
 	for(int i=0; i<2; ++i){

 		for(int j=0; j<4; ++j){

 			output[j][i] = input[i][j];
 		}
 	}
 }


 int main()
 {

 	const int a[2][4]= { { 6, 0, 9, 6 } , { 2, 0, 1, 1 } };
 	int b[2][4];
	
 	transpose(a,b);
	cout<<"a"<<a[0][0]<<endl;
 	cout<<"b"<<b[0][0]<<endl;

 	return 0;
 }
