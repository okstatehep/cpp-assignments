/*
 * 3-4.1.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

/* this is to output N/5 if N is nonnegative and divisible by 5 other wise output -1*/

#include<iostream>
using namespace std;

int main(){

	do{
		int N;


	cout<<"enter the number you desire: ";
	cin>>N;

	cout<<((N%5==0 && N>=0)? N/5 : -1)<<endl;
	}
	while(1);
	return 0;
}
