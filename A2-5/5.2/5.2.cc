/*
 * 5.2.cc
 *
 *  Created on: Sep 16, 2015
 *      Author: thilak
 */

/* simulate multiple dart throws.  */

#include<iostream>
#include<cmath>
#include<cstdlib>
using namespace std;

int main(){

	int dartnum=0;
	int n=10;
	for(int i=0; i<n; ++i){


			cout<<rand()<<endl;
			cout<<RAND_MAX<<endl;
			cout<<(double)rand()/RAND_MAX<<endl;

			double x=(double)rand()/RAND_MAX;
			double y=(double)rand()/RAND_MAX;

		/*radius of the circle is 1, therefore in calculating the darts in circle,
		 * x^2+y^2<1
		 */

				if(sqrt(x*x+y*y<=1)){

			++dartnum;

		}

	}

		cout<<"number of darts in the circle: "<<dartnum<<endl;

	return 0;
}
