/*
 * 2-2.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

#include<iostream>
using namespace std;

/* this is to print Hello World n times using, 1. for loop, 2. a while loop, 3. a do-while loop  */




int main(){


	int x;
	int i=0;

	cout<<"Enter the number you want to print Hello World: \v"<<endl;

	cin>>x;

	/* using for loop */

	/*for(i=0; i<x ; i++){

		cout<<"Hello World!"<<endl;
	}*/

	/*-------------------------------------------------------------*/

	/* using while loop */

	/*while(i<x){
		cout<<"Hello World!"<<endl;
		i=i+1;
	}*/

	/*-------------------------------------------------------------*/

	/* using do-while loop */

	do{
		cout<<"Hello World!"<<endl;
		i=i+1;
	}
	while(i<x);

	return 0;
}



