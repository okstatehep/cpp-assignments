/*
 * 5.3.cc
 *
 *  Created on: Sep 16, 2015
 *      Author: thilak
 */



/* this is to calculate Pi. Pi = 4*((a/4)/(r*r)) here r=1 */


#include<iostream>
#include<cmath>
#include<cstdlib>
using namespace std;

int main(){

	srand(time(0));
	int dartnum=0;
	int n=100000;
	for(int i=0; i<n; ++i){


			double x=(double)rand()/RAND_MAX;
			double y=(double)rand()/RAND_MAX;

		/*radius of the circle is 1, therefore in calculating the darts in circle,
		 * x^2+y^2<1
		 */

				if(sqrt(x*x+y*y<=1)){

			++dartnum;

		}

	}

		cout<<"number of darts in the circle: "<<dartnum<<endl;

		double pi = (double)dartnum/n;

		cout<<"value of Pi = "<<pi<<endl;

	return 0;
}
