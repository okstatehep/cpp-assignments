/*
 * geometry.h
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_

/* 5.1 */
class Polygon{

protected:
	PointArray points;
	static int numPolygons;

public:
	/* 5.2 */
	/* constructor creates polygon with two arguments */
	Polygon(const Point points[], const int numofPoints);
	/* constructor creates a polygon using the points in an exsiting PointArray  */
	Polygon(const PointArray &pa);


/* 5.3 */
virtual double area();
static int getnumPolygons(){
	return numPolygons;
}
int getNumSides(){
	cout<<"Enter the no of sides you like: "<<endl;
	int x;
	cin>>x<<endl;
	return x;
}

const PointArray *getPoints(){
	return &points;
	}

~Polygon(){
	return numPolygons;
	}

};


#endif /* GEOMETRY_H_ */
