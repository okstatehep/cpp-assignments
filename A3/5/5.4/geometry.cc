/*
 * geometry.cc
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */

/*5.4: Rectangle*/

#include<iostream>
#include "geometry.h"

using namespace std;

Point constructorPoints [4];

 Point *updateConstructorPoints ( const Point &p1 , const Point &p2 ,
const Point &p3 , const Point &p4 = Point (0 ,0)) {
 constructorPoints [0] = p1;
 constructorPoints [1] = p2;
 constructorPoints [2] = p3;
 constructorPoints [3] = p4;
 return constructorPoints ;
 }

/* construct with two points */
Rectangle::Rectangle(const Point &ll, const Point ur) : Polygon(updateConstructorPoints(ll, Point(ll.getX(), ur.getY()), ur, Point(ll.getY(), ur.getX())),4){}

 /* construct with four ints */
Rectangle::Rectangle(const int llx, const int lly, const int urx, const int ury) : Polygon(updateConstructorPoints(Point(llx, lly), Point(llx, ury), Point(urx, lly), Point(urx, ury)),4) {}


double Rectangle::area() const{

	double length = ur.getX() - ll.getX();
	double width = ur.getY() - ll.getY();

	return length*width;
}
