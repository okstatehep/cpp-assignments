/*
 * 6.2.cc
 *
 *  Created on: Sep 16, 2015
 *      Author: thilak
 */

/* this is to reverse the content in a given array */

#include<iostream>
using namespace std;

void reverse(int array[], int length){

	for(int i=0; i<length; i++ ){

		cout<<array[i];
		}
// reversing the above
	for(int j=length-1; j>=0; j--){

		cout<<"\n"<<array[j];
	}
}

int main(){

	int a[]={1,2,3,4,5};
	reverse(a,5);

	return 0;
}
