/*
 * 3-4.3.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

/* this is allowing the user to break out from the program by providing any negative value */

#include<iostream>
using namespace std;

int main(){

	do{
			int N;


		cout<<"enter the number you desire: ";
		cin>>N;

		if(N%5==0 && N>=0){

			cout<<N/5<<endl;
			continue;
		}

		else if(N<=-1){

			break;
		}
	}
		while(1);

		cout<<"you can't use the program any more!!!"<<endl;

	return 0;
}
