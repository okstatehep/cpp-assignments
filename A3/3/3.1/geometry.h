/*
 * geometry.h
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */


/* all clss definitions and function prototypes are in this geometry.h file */

class Point {

	//3.1  creating class with two private ints.

private:
	int x;
	int y;

	// 3.2 creating constructors

public:
	Point(int xOri=0, int yOri=0){

		x=xOri;
		y=yOri;

	}

	// 3.3 providing the member functions

	int Point::getX() const {

		return x;
	}

	int Point::getY() const {

			return y;
		}

	void Point::setX(const int xOri){

		x=xOri;
	}

	void Point::setY(const int yOri){

			y=yOri;
		}
};
