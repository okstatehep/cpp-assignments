/*
 * pglatin.cc
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */

#include<iostream>
#include<string>
using namespace std;


int main(){
const string vowels = "aeiou";
int i;
string pigLatinify(const string s){

	/* if "qu" appears at the beginning, it has to be taken and add with "ay" */

	if(s.find("qu")){

	return s.substr(2,s.size()-2)+"-"+s.substr(0,2)+"ay";

	}

	/* if the first letter is a vowel */

	else if(s[0]=="a" || s[0]=="i" || s[0]=="o" || s[0]=="e"|| s[0]=="u"){

		return s + "-way";
	}

	/* if the first letter is a consonant */

	else{

		return s.substr(1,s.size()-1)+"-"+s[0]+"ay";
	}


}
return 0;
}
