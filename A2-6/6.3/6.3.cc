/*
 * 6.3.cc
 *
 *  Created on: Sep 16, 2015
 *      Author: thilak
 */

/* this is to transpose a given matrix */


#include<iostream>
using namespace std;


	int LENGTH=10;
	int WIDTH=10;

	void transpose(const int input[][LENGTH], int output[][WIDTH]){


		for(int i=0; i<LENGTH; ++i){

			for(int j=0; j<WIDTH; ++j){

				input[i][j]=output[j][i];
			}

		}

	}


	int main(){

		int a= input[2][2];
		int b= output[3][3];

		transpose(a,b);

		return 0;
	}
