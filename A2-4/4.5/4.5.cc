/*
 * 4.5.cc
 *
 *  Created on: Sep 16, 2015
 *      Author: thilak
 */

/* single sum function capable to handle arbitrary numbers, take two arguments has a loop and return an integer */

#include<iostream>
using namespace std;


// we can use an array to take any number of integers
int sum(const int array[], const int length){

	long summation=0;

	// summation=a+b+c+d in 4.3 now can be replaced by a for loop s.t loop will execute until it satisfy the user requirement

	for(int i=0; i<length; summation += array[i++])

	return summation;
}

int main(){

 int arr[]={1,2,3,4,5,6,7};

 cout<<"Summation = "<<sum(arr,7)<<endl;

 return 0;
 }
