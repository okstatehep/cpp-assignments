/*
 * geometry.h
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_


// 4.1 creating a class that has the size of the length of the array and the pointer

class PointArray{

private:
	int size;
	Point *points;
// to allow resizing
	void resize(int n);

public:

	// 4.2 constructors

	PointArray();  // this will use to create an array with size 0
	PointArray(const Point points[], const int size);
	PointArray(const PointArray& pv);

	// bringing destructors

	~PointArray();

	// 4.5 introducing member functions

	void PointArray::push_back(const Point &p); // add a point to the end of an array
	void PointArray::insert(const int position, const Point &p); // insert a point at some arbitrary position, shifting the elements to the right
	void PointArray::remove(const int pos); // remove the point at some arbitrary position, shifting the elements to the left
	Point *get(const int pos);
	const Point *get(const int pos) const;

};





#endif /* GEOMETRY_H_ */
