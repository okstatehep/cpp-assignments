/*
 * geometry.cc
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */

#include<iostream>
#include"geometry.h"
#include"cmath"

using namespace std;

/* 5.5 Triangle */

Point constructorPoints [3];

 Point *updateConstructorPoints ( const Point &p1 , const Point &p2 ,
const Point &p3 = Point (0 ,0)) {
 constructorPoints [0] = p1;
 constructorPoints [1] = p2;
 constructorPoints [2] = p3;
 return constructorPoints ;
 }

 Triangle::Triangle(const Point &a, const Point &b, const Point &c) : Polygon(updateConstructorPoints(a, b, c), 3){}

 double Triangle::area() const{


	 double dabx= a.getX() - b.getX();
	 double daby= a.getY() - b.getY();
	 double dacx= a.getX() - c.getX();
	 double dacy= a.getY() - c.getY();
	 double dbcx= b.getX() - c.getX();
	 double dbcy= b.getY() - c.getY();

	 double l1=sqrt((dabx*dabx)+(daby*daby));
	 double l2=sqrt((dacx*dacx)+(dacy*dacy));
	 double l3=sqrt((dbcx*dbcx)+(dbcy*dbcy));

	 double s = (l1+l2+l3)/2;
	 double ar = sqrt(s*(s-l1)*(s-l2)*(s-l3));
	 return ar;
 }
