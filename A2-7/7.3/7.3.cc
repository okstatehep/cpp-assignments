/*
 * 7.3.cc
 *
 *  Created on: Sep 18, 2015
 *      Author: thilak
 */

/* this is replacing references with pointers  */

#include<iostream>
using namespace std;

void swap(int *a, int *b){

	int x = *a;
	*a = *b;
	*b = x;

}

int main(){

	int l=5;
	int m=10;

	swap(l,m);

	cout<<"new l: "<<l<<endl;
	cout<<"new m: "<<m<<endl;

	return 0;
}
