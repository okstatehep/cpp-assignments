/*
 * geometry.h
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_

class Triangle : public Polygon{

public:
	double getX() const;
	double getY() const;
	Triangle(const Point &x,const Point &y, const Point &z);
	virtual double area(); // virtual has the ability to override
};
#endif /* GEOMETRY_H_ */
