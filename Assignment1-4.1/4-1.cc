/*
 * 4-1.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

/* This is the beginning of debugging exercise */

#include<iostream>
using namespace std;


int main(){

	short number;
	cout<<"Enter a number";
	cin>>number;

	cout<<"The factorial of "<<number<<" is";
	int accumulator =1;

	for(;number>0;accumulator *= number--){

		cout<<accumulator<<".\n";

		if(accumulator<=-1){

			cout<<""<<endl;
		}
	}



	return 0;
}


/* 0 gives factorial to be 0 */
/* 1 gives factorial to be 1 */
/* 2 gives factorial to be 1 and then prints 2 */
/* 9 gives factorial to be 1 and then prints 9. 72. 504. 3024. 15120. 60480. 181440. 362880. */
/* 10 gives factorial to be 1 and then prints 10. 90. 720. 5040. 30240. 151200. 604800. 1814400. 3628800.  */
