/*
 * 7.1.cc
 *
 *  Created on: Sep 18, 2015
 *      Author: thilak
 */

/* return the length of a string(char *), excluding the final NULL character. not  using stdlib functions and indexing operator */

#include<iostream>
using namespace std;

int lengthOfString(const char *string){


	int i=0;

	for(; *string++; i++);


	return i;
}
