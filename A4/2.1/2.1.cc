/*
 * 2.1.cc
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */
#include<iostream>

template <typename T>
T const min(T const a, T const b){

	return a < b ? a:b;
}

int main(){

	int x=5;
	int y=10;

	std::cout<<"minimum of the given integers is: "<<min(x,y)<<std::endl;

	return 0;
}
