/*
 * 3-4.2.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

/* This is to modify the example 3.4.1 s.t if the condition fais nothing will printed. Also the ternary operator is replaced by "if" and "continue" */

#include<iostream>
using namespace std;

int main(){

	do{
			int N;


		cout<<"enter the number you desire: ";
		cin>>N;

		if(N%5==0 && N>=0){

			cout<<N/5<<endl;
			continue;
		}
	}
		while(1);


	return 0;
}
