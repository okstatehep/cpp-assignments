/*
 * 6.1.cc
 *
 *  Created on: Sep 16, 2015
 *      Author: thilak
 */

#include<iostream>
using namespace std;

void printArray(int array[], int length){

	for(int i=0; i<length; i++ ){


		cout<<array[i];
		// cout<<array[i]<<"," print "," after the last integer so

		if(i<length-1){
			cout<<",";
		}
	}
}

int main(){

	int a[]={1,2,3,4,5};
	printArray(a,5);

	return 0;
}
