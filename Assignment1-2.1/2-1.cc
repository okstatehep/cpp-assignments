/*
 * 2-1.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

/* This is to write Hello World using char* */

#include<iostream>

using namespace std;

int main()
{

	const char* text="Hello World!";

	cout<<text<<endl;

	return 0;
}
