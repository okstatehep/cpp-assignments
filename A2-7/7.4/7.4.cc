/*
 * 7.4.cc
 *
 *  Created on: Sep 18, 2015
 *      Author: thilak
 */


/* here, instead of swaping two values, two pointers should swap to each others values */


#include<iostream>
using namespace std;

/* now we have to swap two pointers so following what we did in 7.3 we can point the pointer to the others value as follows.
 * since now we are pointing at the values, l from 7.2 will become a pointer as *l.  */

void swap(int **a, int **b){

	int *l = *a;
	*a = *b;
	*b = l;

}

int main(){

	int x=5;
	int y=6;

	int *ptr1 = &x;
	int *ptr2 = &y;

	swap( &ptr1, &ptr2 );

	cout<< *ptr1 <<" , "<< *ptr2<<endl;


	return 0;
}
