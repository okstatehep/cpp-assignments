/*
 * geometry.h
 *
 *  Created on: Oct 4, 2015
 *      Author: thilak
 */

#ifndef GEOMETRY_H_
#define GEOMETRY_H_


class Rectangle : public Polygon{

public:
	double getX() const;
	double getY() const;
	Rectangle(const Point &x,const Point &y);
	Rectangle(const int a, const int b, const int c, const int d);
	virtual double area(); // virtual has the ability to override
};


#endif /* GEOMETRY_H_ */
