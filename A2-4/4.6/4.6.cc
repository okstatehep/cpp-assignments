/*
 * 4.6.cc
 *
 *  Created on: Sep 17, 2015
 *      Author: thilak
 */

/* loop in previous question is replaced here by a recursion */

#include<iostream>
using namespace std;


// we can use an array to take any number of integers
int sum(const int array[], const int length){

	//long summation=0;

	if(length==0){

		return 1;
	}
		else{
			return array[1]+sum(array+1,length-1);
		}


}

int main(){

 int arr[]={1,2,3,4,5,6,7,8,10};

 cout<<"Summation = "<<sum(arr,9)<<endl;

 return 0;
 }
