/*
 * 3.2.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: thilak
 */

/* This program is to determine the maximum, minimum, average and the range of a given list of integers */

#include<iostream>
using namespace std;

int main(){

	int N;
	cout<<"enter the number of times"<<endl;
	cin>>N;// this will decide how many numbers allowed to input for the calculation


	int a=0;
	cin>>a; // this will take the input from user and assign to min and max to begin the program
	int min=a;
	int max=a;

	for(int i =1; i < N; ++i){

		int b;
		cin>>b;  // this is the variable that take the input from the user
		a+=b; // here, a will increase according to the input until the cycle completed

		if(b<min){
			min=b;
		}
		else if(b>max){
			max=b;
		}
	}

	cout<<"Minimum = "<<min<<endl;
	cout<<"Maximum = "<<max<<endl;
	cout<<"Mean = "<<(double) a/N<<endl;
	cout<<"Range = "<<(max-min)<<endl;

	return 0;
}
